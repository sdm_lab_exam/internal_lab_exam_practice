const mysql = require('mysql2')

const createDatabaseConnection = ()=>{
    const connection = mysql.createConnection({
        host:'db',  // this will be the logical(not real) name of your container where db is running, which will be given by docker-compose and this name can be anything excpet localhost and you don't have to do anything with db( db.sql or db or docker file of your db ), 
 // the respective configuration will be done on docker-compose.yaml not anywhere in db.sql or Dockerfile of db
        port:3306,
        user:'root',
        password:'root',  ////same as dockerfile of DB
        database:'mydb', //same as dockerfile of DB
    })

    connection.connect()

    return connection
}

module.exports={
    createDatabaseConnection
}