const express = require('express')
const db = require('../db')
const util = require('../utils')


const router = express.Router()

router.get('/get',(request ,response)=>{
      const connection = db.createDatabaseConnection()

      const statement = `select * from Emp`

      connection.query(statement, (error, result)=>{
          connection.end()
          console.log(result)
          if(result.length>0)
          {
             response.send(util.createResult(error,result))
          }
          else
          {
              response.send('user not found!')
          }
      })
})

router.post('/add', (request, response)=>{
    const connection = db.createDatabaseConnection()

    const {name, salary , age  } = request.body

    const statement = `insert into Emp(name,salary,age) values('${name}', '${salary}', '${age}')`

    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(util.createResult(error,result))
    })
})

router.put('/update/:id', (request, response)=>{
    const {id } = request.params
    const {salary} = request.body

    const connection = db.createDatabaseConnection()

    const statement = `update Emp set salary ='${salary}' where id = '${id}'`

    connection.query(statement, (error, result)=>{
        connection.end()
        response.send(util.createResult(error, result))
    })
})


router.delete('/delete/:id', (request, response)=>{
    const {id} = request.params

    const connection = db.createDatabaseConnection()

    const statement = `delete from Emp where id = '${id}'`

    connection.query(statement, (error, result)=>{
         connection.end()
         response.send(util.createResult(error, result))
    })
})


module.exports = router