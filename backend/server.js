const express = require('express')
const routerEmp = require('./routes/emp')

const app = express()
const cors = require('cors')


app.use(express.json())
app.use(cors('*'))
app.use('/emp',routerEmp)

app.listen(4000, "0.0.0.0", ()=>{
    console.log('server started on port 4000')
})

